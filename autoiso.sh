	##* CUSTOMIZING AND PRESEEDING UBUNTU LIVECDs v2 *##
	# https://nathanpfry.com/how-to-customize-an-ubuntu-installation-disc/

	## Variables ##
originaliso=$"ubuntu-18.04.2-desktop-amd64.iso"
originaliso=$"ISOs/ubuntu-19.04-desktop-amd64.iso"
originaliso=$"ubuntu19-custom-image.iso"
originaliso=$"ubuntu19Mod.iso"
originaliso=$"~/repositories/custom-img/ISOs/ubuntu19-custom-image.iso"
mver=$"18"
mver=$"19"
mver=$"18Mod"
mver=$"19Modv2"

	# ___________________________

	# First download the ISO you want to start customizing from Ubuntu’s release server.
	
	# Prerequisite software to be installed.

#sudo apt-get install squashfs-tools genisoimage

	# Create a fresh folder to begin work. 

#sudo mkdir ~/custom-img

	# ISOs folder

#sudo mkdir ~/repositories/custom-img/ISOs

	# Move the base ISO downloaded in the first step to the working directory.

#cp ~/repositories/custom-img/ISOs/$originaliso ~/custom-img
cd ~/custom-img

	# Next, extract the contents of disc image.

sudo mkdir mnt
sudo mount -o loop $originaliso mnt
sudo mkdir extract
sudo rsync --exclude=/casper/filesystem.squashfs -a mnt/ extract

	# Extract the filesystem with the following commands:

sudo unsquashfs mnt/casper/filesystem.squashfs
sudo mv squashfs-root edit

	# Copy resolv.conf from your system into the freshly unpacked fs for network access.

sudo cp /etc/resolv.conf edit/etc/

	# Mount a few important working directories:

sudo mount --bind /dev/ edit/dev
sudo chroot edit
mount -t proc none /proc
mount -t sysfs none /sys
mount -t devpts none /dev/pts

	# Commands will make sure that everything goes smoothly while modifying packages.

export HOME=/root
export LC_ALL=C
dbus-uuidgen > /var/lib/dbus/machine-id
dpkg-divert --local --rename --add /sbin/initctl
ln -s /bin/true /sbin/initctl

		## Removing Aps Packages ##

apt-get purge -y mythes-de-ch/disco mythes-de/disco mythes-en-au/disco mythes-en-us/disco mythes-es/disco mythes-fr/disco mythes-it/disco mythes-pt-pt/disco mythes-ru/disco

apt-get purge -y thunderbird-locale-de/disco thunderbird-locale-en-gb/disco thunderbird-locale-en/disco thunderbird-locale-es-ar/disco thunderbird-locale-es-es/disco thunderbird-locale-es/disco thunderbird-locale-fr/disco thunderbird-locale-it/disco thunderbird-locale-pt-br/disco thunderbird-locale-pt-pt/disco thunderbird-locale-pt/disco thunderbird-locale-ru/disco thunderbird-locale-zh-cn/disco thunderbird-locale-zh-hans/disco thunderbird-locale-zh-hant/disco thunderbird-locale-zh-tw/disco libreoffice-help-de/disco libreoffice-help-en-gb/disco libreoffice-help-en-us/disco libreoffice-help-es/disco libreoffice-help-fr/disco libreoffice-help-it/disco libreoffice-help-pt-br/disco libreoffice-help-pt/disco libreoffice-help-ru/disco libreoffice-help-zh-cn/disco libreoffice-help-zh-tw/disco 
libreoffice-l10n-de/disco libreoffice-l10n-en-gb/disco libreoffice-l10n-en-za/disco libreoffice-l10n-es/disco libreoffice-l10n-fr/disco libreoffice-l10n-it/disco libreoffice-l10n-pt-br libreoffice-l10n-pt/disco libreoffice-l10n-ru/disco libreoffice-l10n-zh-cn/disco libreoffice-l10n-zh-tw/disco language-pack-de-base/disco language-pack-de/disco language-pack-es-base/disco language-pack-es/disco language-pack-fr-base/disco language-pack-fr/disco language-pack-gnome-de-base/disco language-pack-gnome-de/disco language-pack-gnome-es-base/disco language-pack-gnome-es/disco language-pack-gnome-fr-base/disco language-pack-gnome-fr/disco language-pack-gnome-it-base/disco language-pack-gnome-it/disco language-pack-gnome-pt-base/disco language-pack-gnome-pt/disco language-pack-gnome-ru-base/disco language-pack-gnome-ru/disco language-pack-gnome-zh-hans-base/disco language-pack-gnome-zh-hans/disco language-pack-it-base/disco language-pack-it/disco language-pack-pt-base/disco language-pack-pt/disco language-pack-ru-base/disco language-pack-ru/disco language-pack-zh-hans-base/disco language-pack-zh-hans/disco wbrazilian/disco wbritish/disco wfrench/disco wportuguese/disco wspanish/disco wswiss/disco hunspell-de-at-frami/disco hunspell-de-ch-frami/disco hunspell-de-de-frami/disco hunspell-en-au/disco hunspell-en-gb/disco hunspell-en-za/disco hunspell-es/disco hunspell-fr-classical hunspell-fr/disco hunspell-it/disco hunspell-pt-br/disco hunspell-pt-pt/disco hunspell-ru/disco hyphen-de/disco hyphen-en-gb/disco hyphen-es/disco hyphen-fr/disco hyphen-it/disco hyphen-pt-br/disco hyphen-pt-pt/disco hyphen-ru/disco gnome-user-docs-de/disco gnome-user-docs-es/disco gnome-user-docs-fr/disco gnome-user-docs-it/disco gnome-user-docs-pt/disco gnome-user-docs-ru/disco gnome-user-docs-zh-hans/disco gnome-getting-started-docs-de/disco gnome-getting-started-docs-es/disco gnome-getting-started-docs-fr/disco gnome-getting-started-docs-it/disco gnome-getting-started-docs-pt/disco gnome-getting-started-docs-ru/disco

dpkg -P firefox-locale-de firefox-locale-es firefox-locale-fr firefox-locale-it firefox-locale-pt firefox-locale-ru firefox-locale-zh-hans


		## Multi Arch Support ##

dpkg --add-architecture i386

	# Update the software repositories and upgrade the remaining packages on the system.

apt-get update && apt-get upgrade

	## Add packages ##

	# Adds Vim
apt-get install vim
	# Google Chrome browser
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo apt install ./google-chrome*.deb
	# Adds chrome shortcut
#sudo cp google-chrome.desktop ~/.local/share/applications/google-chrome.desktop
	# Installs gimp
#sudo add-apt-repository -y ppa:otto-kesselgulasch/gimp
#sudo apt update
#sudo apt-get -y install gimp
	# Installs Adobe Reader
wget ftp://ftp.adobe.com/pub/adobe/reader/unix/9.x/9.5.5/enu/AdbeRdr9.5.5-1_i386linux_enu.deb
apt install ./AdbeRdr9.5.5-1_i386linux_enu.deb #**
#sudo apt-get -y install gdebi-core
#sudo gdebi --non-interactive Adbe*.deb
#sudo apt-get -y install libxml2:i386 libstdc++6:i386 libcanberra-gtk-module libcanberra-gtk3-module
#acroread # runs Adobe Reader for first time so that user can accept terms and conditions
	# Installs Microsoft fonts
#sudo apt install ttf-mscorefonts-installer
sudo fc-cache -f -v

	# You are almost there! Time to clean up:

apt-get autoremove && apt-get autoclean
rm -rf /tmp/* ~/.bash_history
rm /var/lib/dbus/machine-id
rm /sbin/initctl
dpkg-divert --rename --remove /sbin/initctl

	# Unmount the directories from the beginning of this guide:

umount /proc || umount -lf /proc
umount /sys
umount /dev/pts
exit
sudo umount edit/dev

	# Generate a new file manifest:

sudo chmod +w extract/casper/filesystem.manifest

sudo chroot edit dpkg-query -W --showformat='${Package} ${Version}\n' | sudo tee extract/casper/filesystem.manifest

sudo cp extract/casper/filesystem.manifest extract/casper/filesystem.manifest-desktop

sudo sed -i '/ubiquity/d' extract/casper/filesystem.manifest-desktop

sudo sed -i '/casper/d' extract/casper/filesystem.manifest-desktop

	# Compress the filesystem:

sudo mksquashfs edit extract/casper/filesystem.squashfs -b 1048576

	# Update fi lesystem size (needed by the installer):

printf $(sudo du -sx --block-size=1 edit | cut -f1) | sudo tee extract/casper/filesystem.size

	# Delete the old md5sum:

cd extract
sudo rm md5sum.txt

	# …and generate a fresh one: (single command, copy and paste in one piece)

find -type f -print0 | sudo xargs -0 md5sum | grep -v isolinux/boot.cat | sudo tee md5sum.txt


#________________________________________________

	# Nano ~/repositories/custom-img/extract/isolinux/isolinux.cfg

sudo nano ~/repositories/custom-img/extract/isolinux/isolinux.cfg

default live-install
label live-install
  menu label ^Install Ubuntu
  kernel /casper/vmlinuz
  append  file=/cdrom/ks19.preseed auto=true priority=critical debian-installer/locale=en_US keyboard-configuration/layoutcode=us ubiquity/reboot=true languagechooser/language-name=English countrychooser/shortlist=US localechooser/supported-locales=en_US.UTF-8 boot=casper automatic-ubiquity initrd=/casper/initrd quiet splash noprompt noshell ---

	# Copied custom pressed file from the ~/repositories/ganeti/ks19.preseed to the root of the extract/ubuntu$mver

sudo cp ~/repositories/vcn_computer_setup/ks19.preseed ~/repositories/custom-img/extract/
	#________________________________________________

sudo genisoimage -D -r -V "UBUNTU$mver" -cache-inodes -J -l -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -o ~/repositories/custom-img/ISOs/UBUNTU$mver.iso .


	# Iso Hybrid command (to boot)
sudo isohybrid ~/repositories/custom-img/ISOs/ubuntu$mver.iso

	# Single boot writing ISO image to target USB disk (will destroy data on USB disk):

sudo multibootusb -c -r -i ~/repositories/custom-img/ISOs/ubuntu$mver.iso -t /dev/sdb


	#________________________________________________

	# umount /dev/loop** and Removing Files
sudo umount /dev/loop18
sudo rm -rf ~/repositories/custom-img/mnt
sudo rm -rf ~/repositories/custom-img/extract
sudo rm -rf ~/repositories/custom-img/edit

