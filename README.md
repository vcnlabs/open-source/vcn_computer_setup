#Summary
To create a new Ubuntu distribution by producing a combination of custom iso and a preseed configuration file that 
    ## produces reports of the OS, apps, network and hardware, 
    ## updates and upgrades the Ubuntu distro and version;
    ## Makes accounts and furbish them with goodies;
    ## Removes extra packages ie. Language packs;
    ## adds software packages like chrome, Adobe Reader, vim, gimp, microsoft-fonts
    ## updates apps packages
    ## changes system defaults (theme, icons, desktop background, panels, browser homepage, etc) 
